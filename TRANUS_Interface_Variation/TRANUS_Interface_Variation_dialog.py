# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TRANUSInterfaceVariationDialog
                                 A QGIS plugin
 This plugin allows making successive iterations of TRANUS, with variations on a single variable.
                             -------------------
        begin                : 2017-04-19
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Inria
        email                : peter.sturm@inria.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt4 import QtGui, uic, QtCore
from PyQt4.Qt import QMessageBox, QVariant
import launch_variation_dialog



FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'TRANUS_Interface_Variation_dialog_base.ui'))


class TRANUSInterfaceVariationDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self,project,settings,tranus_project,shape_file,tranus_binaries,parent=None):
        """Constructor of the first main window"""
        super(TRANUSInterfaceVariationDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
        self.project = project
        
        self.settings = settings
        self.tranus_project = tranus_project
        self.base_scenario = ""
        self.tranus_binaries = tranus_binaries
        self.shape_file = shape_file
        

        self.help = self.findChild(QtGui.QPushButton, 'btn_help')
        self.layers_group_name = self.findChild(QtGui.QLineEdit, 'layers_group_name')
        self.tranus_workspace = self.findChild(QtGui.QLineEdit, 'tranus_workspace')
        self.tranus_workspace_btn = self.findChild(QtGui.QToolButton, 'tranus_workspace_btn')
        self.load_scenario_btn = self.findChild(QtGui.QPushButton, 'load_scenario')
        self.scenario = self.findChild(QtGui.QLineEdit,'scenario')
        self.tranus_bin_path = self.findChild(QtGui.QLineEdit, 'tranus_bin_path')
        self.tranus_bin_path_btn = self.findChild(QtGui.QToolButton, 'tranus_bin_path_btn')
        self.zones_shape = self.findChild(QtGui.QLineEdit, 'zones_shape')
        self.zones_shape_btn = self.findChild(QtGui.QToolButton,'zones_shape_btn')
        self.load_shape_btn = self.findChild(QtGui.QPushButton, 'load_shape')
        self.close_btn = self.findChild(QtGui.QPushButton, 'btn_close')
        self.launch_btn = self.findChild(QtGui.QCommandLinkButton, 'btn_launch')
        
        #control actions 
        
        self.help.clicked.connect(self.open_help)
        self.tranus_workspace_btn.clicked.connect(self.select_tranus_workspace)
        self.load_scenario_btn.clicked.connect(self.load_base_scenario)
        self.zones_shape_btn.clicked.connect(self.select_shape(self.select_zones_shape))
        self.load_shape_btn.clicked.connect(self.load_shapes)
        self.tranus_bin_path_btn.clicked.connect(self.select_bin_path)
        self.close_btn.clicked.connect(self.close)
        self.launch_btn.clicked.connect(self.launch_TRANUS_variation)
        
        
    def open_help(self):
        """open the file plugins_documentation """
        file = os.path.join(os.path.dirname(os.path.realpath(__file__)),"help\plugins_documentation.pdf")
        os.startfile(file)
    
    def save_settings(self): 
        """save the settings in a config file"""    
        
        self.settings.setValue("tranus_project",self.tranus_workspace.text())
        self.settings.setValue("shape_file",self.zones_shape.text())
        self.settings.setValue("tranus_binaries",self.tranus_bin_path.text())
       
    def save_layers_group_name(self):
        """Saves layers group name, not used for the moment"""
        
        self.project['project_name'] = self.layers_group_name.text()
        self.check_configure()
        
    def select_tranus_workspace(self):
        """ select tranus workspace and save it to the config file"""
        folder = QtGui.QFileDialog.getExistingDirectory(self, "Select directory")
        if folder:
            self.tranus_project = folder
            self.tranus_workspace.setText(self.tranus_project) 
            if not self.project.load_tranus_folder(self.tranus_project):
                QMessageBox.warning(None, "Invalid workspace", "Please select a valid tranus project")
                print "Invalid workspace, Please select a valid tranus project"
                self.tranus_workspace.setText('')
            self.save_settings()
        self.check_configure()
        
    def load_base_scenario(self):
        
        """load base scenario, error message if not"""
        
        self.project.load_tranus_folder(self.tranus_project)
        if self.project.tranus_project:
            self.base_scenario = self.project.tranus_project.base_scenario
            self.scenario.setText(self.base_scenario.name)
        else : 
            self.base_scenario = None
            self.scenario.setText("")
            QMessageBox.warning(None, "Invalid workspace", "Please select a valid tranus project")
            print "Invalid workspace, Please select a valid tranus project"
        self.check_configure()
        
    def select_zones_shape(self,file_name):
        
        """Loads selected zone shape file using path and name of the shape file and save it to the config file"""  
         
        self.shape_file = file_name
        self.zones_shape.setText(self.shape_file)  
        self.save_settings()
        self.check_configure()
            
                
    def select_shape(self,callback):
        
        """ Opens selected zone shape file """   
         
        def select_file():
            file_name = QtGui.QFileDialog.getOpenFileName(parent=self, caption="Select file", filter="*.*, *.shp")
            if file_name:
                callback(file_name)
        return select_file
    
    def load_shapes(self):
        
        """ load shapes, if not error message and save the blank to the config file"""
         
        result, zoneShapeFieldNames = self.project.load_zones_shape(self.shape_file)
        if not result :
            self.zones_shape.setText('')
            self.save_settings()
            QMessageBox.warning(None, "Invalid shape file", "Please select a valid shape file")
            print "Invalid shape file, Please select a valid shape file"
            
        self.check_configure()  
        
    def select_bin_path(self):
        
        """ method used to select the path for the tranus binaries and save it to the config file"""
        
        self.tranus_binaries =  QtGui.QFileDialog.getExistingDirectory(self, "Select directory")
        if self.tranus_binaries:
            self.tranus_bin_path.setText(self.tranus_binaries)
            if not os.path.isfile(os.path.join(self.tranus_binaries,'lcal.exe')) or not os.path.isfile(os.path.join(self.tranus_binaries,'pasos.exe')) :
                QMessageBox.warning(None, "Tranus binaries error", "Tranus binaries not found in : %s"%self.tranus_binaries)
                print'Tranus binaries not found in : %s'%self.tranus_binaries
                self.tranus_bin_path.setText('')
            self.save_settings()
        self.check_configure()    
   
    def launch_TRANUS_variation(self):
        
        """ method used to launch the second GUI interface which is the launch variation, self.accept() is used to close the main window and to launch plot windows"""
        
        dialog = launch_variation_dialog.LaunchVariationDialog(self.tranus_project,self.base_scenario.code,self.tranus_binaries,self.shape_file,parent=self) 
        
        """other equivalent methods
        self.close()
        self.hide()"""
        
        self.accept()
        dialog.show()
        result = dialog.exec_()
    
    def show(self):
        
        """ method used to show the main window with the correct fields"""
         
        self.project.load()
        
        if self.project['project_name']:
            self.layers_group_name.setText(self.project['project_name'])
        else:
            self.layers_group_name.setText('Tranus Variation')
            self.layers_group_name.selectAll()
        if self.project['zones_shape']:
            self.zones_shape.setText(self.project['zones_shape'])
        if self.project.tranus_project:
            self.tranus_workspace.setText(self.project.tranus_project.path)
             
        self.check_configure()
             
    def check_configure(self):
        """ method used to enable the launch button if the tranus bin path is correct and the shapefile and the tranus workspace are correct """
        
        self.launch_btn.setEnabled(self.project.is_valid() and self.tranus_bin_path.text() !='')
      
    
    
               
    