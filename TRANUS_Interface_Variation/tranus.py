# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TRANUSInterfaceVariationDialog
                                 A QGIS plugin
 This plugin allows making successive iterations of TRANUS, with variations on a single variable.
                             -------------------
        begin                : 2017-04-19
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Inria
        email                : peter.sturm@inria.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """
from __future__ import unicode_literals

import os
import re
import csv
import glob

__ALL__ = ['TranusProject', 'TranusProjectValidationError']

# constants definition... from 0 to 4

SECTION_IDENTIFICATION, SECTION_SCENARIOS, SECTION_MODEL, SECTION_SEPARATOR, SECTION = range(5)


def extract_values(line):
    return re.findall(r"\'(.+?)\'", line)


class Identification(object):
    @staticmethod
    def load(lines):
        identification = Identification()
        identification.code, identification.name = Identification.parse_lines(lines)
        return identification

    def validate(self):
        pass

    @staticmethod
    def parse_lines(lines):
        values = extract_values(lines[1])
        return list(values)

    def __str__(self):
        return "%s - %s" % (self.code, self.name)

class BaseScenario(object):
    """ class used to get the base scenario from the scenarios tree"""    
    @staticmethod
    def load(lines):
        
        scenario = BaseScenario()
        scenario.code,scenario.name = BaseScenario.parse_lines(lines)
        return scenario
    
    @staticmethod
    def parse_lines(lines):
        
        values = extract_values(lines[1])
        base_scenario = list(values)[0:2]
        return base_scenario
       
    def __str__(self):
        return "%s - %s" % (self.code,self.name)          
   
class TranusProject(object):
    #scenarios = None
    identification = None
    base_scenario = None
    #model = None
    @staticmethod
    def load_project(file_path):
        project = TranusProject()
        current_section = None
        section_lines = None
        with open(file_path, 'r') as project_file:
            for line in project_file.readlines():
                section = TranusProject.get_section(line)
                if section in (SECTION_IDENTIFICATION, SECTION_SCENARIOS) :
                    if section_lines is not None:
                        project.add_lines_to_section(section_lines, current_section)
                    current_section = section
                    section_lines = []
                elif section == SECTION and section_lines is not None:
                    section_lines.append(line)
            project.add_lines_to_section(section_lines, current_section)
            
           
        project.validate()
        #project.scenarios.load_results(os.path.dirname(file_path))
        project.path = file_path
        return project

    @staticmethod
    def get_section(line):
        if line.startswith('1.0'):
            return SECTION_IDENTIFICATION
        if line.startswith('2.0'):
            return SECTION_SCENARIOS
        if line.startswith('*'):
            return SECTION_SEPARATOR
        return SECTION

    def validate(self):
        try:
            self.identification.validate()
        except AttributeError as e:
            raise TranusProjectValidationError()

    def add_lines_to_section(self, lines, section):
        if section == SECTION_IDENTIFICATION:
            self.identification = Identification.load(lines)
        elif section == SECTION_SCENARIOS:
           self.base_scenario = BaseScenario.load(lines)
    
        
    def __str__(self):
        
        return self.identification.name
        return self.base_scenario.code+'-'+self.base_scenario.name

    def __repr__(self):
        return "<TranusProject: {0}>".format(self.identification)


class TranusProjectValidationError(Exception):
    pass
