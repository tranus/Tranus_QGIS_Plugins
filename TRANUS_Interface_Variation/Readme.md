 Note : A detailed documentation is displayed on the file plugins_documentation.pdf
 Requirements to run this plugin :
 
- Install Pandas library
- Install Matplotlib library

To install these packages you can use pip (a package used to install software packages written in Python). For that, go to: https://bootstrap.pypa.io/get-pip.py  and save it as get-pip.py file. In your OSGeo4W shell, execute this file by typing python get-pip.py.
Now, you can install Pandas. In your OSGeo4W shell(see QGIS installation), type pip install pandas. If you get this error: ValueError: numpy.dtype has the wrong size, try recompiling, then you have to update Numpy by taping pip install --upgrade numpy, you must have the same Numpy version as Pandas.
To install matplotlib, type pip install matplotlib in your OSGeo4W shell.
