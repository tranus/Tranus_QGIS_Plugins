# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TRANUSInterfaceVariation
                                 A QGIS plugin
 This plugin allows making successive iterations of TRANUS, with variations on a single variable.
                             -------------------
        begin                : 2017-04-19
        copyright            : (C) 2017 by Inria
        email                : peter.sturm@inria.fr
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load TRANUSInterfaceVariation class from file TRANUSInterfaceVariation.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .TRANUS_Interface_Variation import TRANUSInterfaceVariation
    return TRANUSInterfaceVariation(iface)
