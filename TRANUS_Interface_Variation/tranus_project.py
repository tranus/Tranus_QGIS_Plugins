# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TRANUSInterfaceVariationDialog
                                 A QGIS plugin
 This plugin allows making successive iterations of TRANUS, with variations on a single variable.
                             -------------------
        begin                : 2017-04-19
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Inria
        email                : peter.sturm@inria.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """


from os import listdir
from os.path import isfile, join


from .tranus import TranusProject
from .tranus import  BaseScenario

from qgis.core import QgsVectorLayer, QgsFeature, QgsGeometry, QgsMapLayerRegistry, QgsField, QgsFeature, QgsSymbolLayerV2Registry, QgsSingleSymbolRendererV2, QgsRendererRangeV2, QgsStyleV2, QgsGraduatedSymbolRendererV2 , QgsSymbolV2, QgsVectorJoinInfo 

from PyQt4.QtCore import QVariant
from PyQt4.QtGui import QColor

from qgis.core import QgsMessageLog  # for debugging
from PyQt4.Qt import QMessageBox


import os
import re
import random
import string
import numpy as np
import csv
import math
from .TRANUS_Interface_Variation_dialog import TRANUSInterfaceVariationDialog




class TranusVariationProject(object):
    def __init__(self, proj):
        
        """Constructor """
        
        self.proj = proj
        self.tranus_project = None
        self.shape = None
       
        self.load()
        

    def load(self):
        
        """Load method"""
        
        self.tranus_project = None
        self.load_tranus_folder()
        self.load_shapes()
      
    
    def addZonesLayer(self,layerName,sectorName,fieldName,zoneList,old_csv):
        
        """Adds new zone layer to project """
  
        registry = QgsMapLayerRegistry.instance()
        layersCount = len(registry.mapLayers())
        group = self.get_layers_group()
        layer = QgsVectorLayer(self.shape, layerName, 'ogr')
        registry.addMapLayer(layer, False)
        if not layer.isValid():
            self['zones_shape'] = ''
            self['zones_shape_id'] = ''
            return False
        
        # Gets shape's file folder
        projectPath = self.shape[0:max(self.shape.rfind('\\'), self.shape.rfind('/'))]
        print (projectPath)
        
        # Gets fields name
        sectorName = sectorName.strip()
        layerName = layerName.encode('UTF-8')
        
        # Creation of CSV file to be used for JOIN operation
        result, minValue, maxValue, rowCounter = self.create_csv_file(layerName,sectorName,fieldName,projectPath,zoneList,old_csv)
        if result:
            # to be compatible with mac os
            csvFile_uri = os.path.join("file:///"+projectPath,layerName + ".csv?delimiter=,")
            print (csvFile_uri)
            csvFile = QgsVectorLayer(csvFile_uri, layerName, "delimitedtext")
            registry.addMapLayer(csvFile, False)
            shpField = 'zoneID'
            csvField = 'ZoneId'
            joinObject = QgsVectorJoinInfo()
            joinObject.joinLayerId = csvFile.id()
            joinObject.joinFieldName = csvField
            joinObject.targetFieldName = shpField
            joinObject.memoryCache = True
            layer.addJoin(joinObject)
            
            print(minValue, maxValue, rowCounter)
            
            myStyle = QgsStyleV2().defaultStyle()
            defaultColorRampNames = myStyle.colorRampNames()        
            ramp = myStyle.colorRamp(defaultColorRampNames[0])
            ranges  = []
            nCats = ramp.count()
            print("Total colors: "+str(nCats))
            rng = maxValue - minValue
            red0 = 255
            red1 = 0
            green0 = 255
            green1 = 0
            blue0 = 255
            blue1 = 255
            nCats = 50
            for i in range(0,nCats):
                v0 = minValue + rng/float(nCats)*i
                v1 = minValue + rng/float(nCats)*(i+1)
                symbol = QgsSymbolV2.defaultSymbol(layer.geometryType())
                red = red0 + float(i)/float(nCats-1)*(red1-red0)
                green = green0 + float(i)/float(nCats-1)*(green1-green0)
                blue = blue0 + float(i)/float(nCats-1)*(blue1-blue0)
                symbol.setColor(QColor(red, green, blue))
                myRange = QgsRendererRangeV2(v0,v1, symbol, "")
                ranges.append(myRange)
                 
            if fieldName =="Shadow price":
                field = "Adjust" 
            elif fieldName =="Absolute value of shadow price":
                field = "AbsAdjust"
            elif fieldName =="Ratio shadow price by price":
                field = "AdjustPerPrice"
            elif fieldName =="Ratio shadow price by price":
                field = "AdjustPerPrice"
            elif fieldName =="Total production":
                field = "TotProd"
            elif fieldName =="Total demand":
                field = "TotDem"
            elif fieldName =="Production cost":
                field = "ProdCost"
            elif fieldName =="Price":
                field = "Price"
            elif fieldName =="Minimum restriction":
                field = "MinRes"
            elif fieldName =="Maximum restriction":
                field = "MaxRes"
            
            # to be compatible with mac os
            s=""   
            seq = (layerName,"_",field)
            s = s.join(seq)
            renderer = QgsGraduatedSymbolRendererV2(s, ranges)
            
            renderer.setSourceColorRamp(ramp)
            layer.setRendererV2(renderer)

            group.insertLayer((layersCount+1), layer)
            self['zones_shape'] = layer.source()
            self['zones_shape_id'] = layer.id()
            
        return True
    
    def load_tranus_folder(self, folder=None):
        
        """Loads tranus project folder"""
        
        folder = folder or self['tranus_folder']
        
        path = os.path.join(folder, 'W_TRANUS.CTL')
        print (path)
        try:
            tranus_project = TranusProject.load_project(path)
        except Exception as e:
            print (e)
            self.tranus_project = None
            return False
        else:
            self.tranus_project = tranus_project
            self['tranus_folder'] = folder
            return True

                
    def load_zones_shape(self,shape):
        
        """ Loads zone shape"""
        
        self.shape = shape
        registry = QgsMapLayerRegistry.instance()
        group = self.get_layers_group()
        layer = QgsVectorLayer(shape, 'Zones', 'ogr')
        if not layer.isValid():
            self['zones_shape'] = ''
            self['zones_shape_id'] = ''
            return False, None
        
        zones_shape_fields = [field.name() for field in layer.pendingFields()]
        
        project = shape[0:max(shape.rfind('\\'), shape.rfind('/'))]     
            
        if self['zones_shape_id']:
            existing_tree = self.proj.layerTreeRoot().findLayer(self['zones_shape_id'])
            if existing_tree:
                existing = existing_tree.layer()
                registry.removeMapLayer(existing.id())

        registry.addMapLayer(layer, False)
        group.insertLayer(0, layer)
        self['zones_shape'] = layer.source()
        self['zones_shape_id'] = layer.id()
        return True, zones_shape_fields 

    
    def __getitem__(self, key):
        value, _ = self.proj.readEntry('TRANUS VARIATION', key)
        return value

    def __setitem__(self, key, value):
        self.proj.writeEntry('TRANUS VARIATION', key, value)

    def is_created(self):
        return not not self['project_name']

    def is_valid(self):
      
        return not not (self['zones_shape'] and self['tranus_folder'])


    def get_layers_group(self):
        
        """ Gets layer group """
        
        group_name = self['layers_group_name'] or 'TRANUS VARIATION'
        layers_group = self.proj.layerTreeRoot().findGroup(group_name)
        if layers_group is None:
            layers_group = self.proj.layerTreeRoot().addGroup(group_name)
        return layers_group

    def load_shapes(self):
        
        """ Loads zone shape """
        
        zones_shape = self['zones_shape']
        layers_group = self.get_layers_group()
        
        for layer in layers_group.findLayers():
            if layer.layer().source() == zones_shape:
                self['zones_shape_id'] = layer.layer().id()
    
    def create_csv_file(self,layerName,sectorName,fieldName,filePath,zoneList,old_csv):
        
        """ create results csv file (resulting from the join operation between shapefile and imploc file) used to represent layers"""
        
        minValue = float(1e100)
        maxValue = float(-1e100)
        rowCounter = 0
        
        if zoneList is None:
            QMessageBox.warning(None, "Zone data", "There is not data to evaluate.")
            print ("There is not data to evaluate.")
            return False, minValue, maxValue, rowCounter
        else:
            rowCounter = len(zoneList)
           
        csvFile = open(os.path.join(filePath,layerName + ".csv"), "wb")
        newFile = csv.writer(csvFile, delimiter=',', quotechar='', quoting=csv.QUOTE_NONE,escapechar='\t')
        print ('Writing CSV File "{0}"'.format(layerName.encode('utf-8')))
        # Write header of the csv file with all layers to represent
        newFile.writerow(["ZoneId","ZoneName","Price","Adjust","AbsAdjust","AdjustPerPrice","TotProd","TotDem","ProdCost","MinRes","MaxRes"])
        with open(old_csv,'rb') as file:
            reader = csv.reader(file, delimiter=',', quotechar='',quoting=csv.QUOTE_NONE)
            for row in reader:
                value = 0
                num_name_sector = row[1].split()
                if len(num_name_sector) == 2 :
                    name_sector = num_name_sector[1]
                    if name_sector == sectorName:
                        
                        zoneId = row[2].split()[0]
                        zoneName = row[2].split()[1]
                        
                        if fieldName == "Shadow price":
                            value = float(row[9]) # adjust
                            abs_adjust,adjust_per_price = self.calc_values(row[6], row[9])
                            newFile.writerow([zoneId,zoneName,row[6],value,abs_adjust,adjust_per_price,row[3],row[4],row[5],row[7],row[8]])  
                            minValue = min (minValue,value)
                            maxValue = max (maxValue,value)
                        
                        elif fieldName == "Absolute value of shadow price":
                            value = math.fabs(float(row[9])) # abs_adjust
                            if float(row[6]) == 0 :
                                adjust_per_price =0
                            else :
                                adjust_per_price = math.fabs(float(row[9])/float(row[6])) #adjust_per_price
                            newFile.writerow([zoneId,zoneName,row[6],row[9],value,adjust_per_price,row[3],row[4],row[5],row[7],row[8]])  
                            minValue = min (minValue,value)
                            maxValue = max (maxValue,value)
                            
                        elif fieldName == "Price":
                            value = float(row[6]) #price
                            abs_adjust,adjust_per_price = self.calc_values(row[6], row[9])
                            newFile.writerow([zoneId,zoneName,value,row[9],abs_adjust,adjust_per_price,row[3],row[4],row[5],row[7],row[8]])  
                            minValue = min (minValue,value)
                            maxValue = max (maxValue,value)
                            
                        elif fieldName =="Ratio shadow price by price":
                            abs_adjust = math.fabs(float(row[9])) # abs_adjust
                            if float(row[6]) == 0 :
                                value = 0
                            else :
                                value = math.fabs(float(row[9])/float(row[6])) #adjust_per_price
                            newFile.writerow([zoneId,zoneName,row[6],row[9],abs_adjust,value,row[3],row[4],row[5],row[7],row[8]])  
                            minValue = min (minValue,value)
                            maxValue = max (maxValue,value)
                            
                        elif fieldName =="Total production":
                            value = float(row[3]) #totprod
                            abs_adjust,adjust_per_price = self.calc_values(row[6], row[9])
                            newFile.writerow([zoneId,zoneName,row[6],row[9],abs_adjust,adjust_per_price,value,row[4],row[5],row[7],row[8]])  
                            minValue = min (minValue,value)
                            maxValue = max (maxValue,value)
                        
                        elif fieldName =="Total demand":
                            value = float(row[4]) #totdemand
                            abs_adjust,adjust_per_price = self.calc_values(row[6], row[9])
                            newFile.writerow([zoneId,zoneName,row[6],row[9],abs_adjust,adjust_per_price,row[3],value,row[5],row[7],row[8]])  
                            minValue = min (minValue,value)
                            maxValue = max (maxValue,value)
                            
                        elif fieldName =="Production cost":
                            value = float(row[5]) #prodcost
                            abs_adjust,adjust_per_price = self.calc_values(row[6], row[9])
                            newFile.writerow([zoneId,zoneName,row[6],row[9],abs_adjust,adjust_per_price,row[3],row[4],value,row[7],row[8]])  
                            minValue = min (minValue,value)
                            maxValue = max (maxValue,value)
                            
                        elif fieldName =="Minimum restriction":
                            value = float(row[7]) #minres
                            abs_adjust,adjust_per_price = self.calc_values(row[6], row[9])
                            newFile.writerow([zoneId,zoneName,row[6],row[9],abs_adjust,adjust_per_price,row[3],row[4],row[5],value,row[8]])  
                            minValue = min (minValue,value)
                            maxValue = max (maxValue,value)
                            
                        elif fieldName =="Maximum restriction":
                            value = float(row[8]) #maxres
                            abs_adjust,adjust_per_price = self.calc_values(row[6], row[9])
                            newFile.writerow([zoneId,zoneName,row[6],row[9],abs_adjust,adjust_per_price,row[3],row[4],row[5],row[7],value])  
                            minValue = min (minValue,value)
                            maxValue = max (maxValue,value)
                        
                                   
                           
        del newFile
        csvFile.close()
        del csvFile
        print("Min: {0}, Max: {1}, Counter: {2}").format(minValue, maxValue, rowCounter)
        return True, minValue, maxValue, rowCounter
    
    def calc_values(self,row_price,row_adjust):
        
        """ method to calculate abs_adjust and adjust per price"""
        
        abs_adjust = math.fabs(float(row_adjust)) #abs_adjust
        if float(row_price) == 0 :
            adjust_per_price = 0
        else :
            adjust_per_price = math.fabs(float(row_adjust)/float(row_price)) #adjust_per_price
                   
        return abs_adjust,adjust_per_price

    
   